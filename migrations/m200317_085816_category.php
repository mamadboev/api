<?php

use yii\db\Migration;

/**
 * Class m200317_085816_category
 */
class m200317_085816_category extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('category', [
            'id' => $this->primaryKey(),
            'c_name' => $this->string(),
            'status'=>$this->integer(1),
        ]);
        $this->createIndex(
            'idx-category-id',
            'category',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-category-id',
            'category'
        );
        $this->dropTable('category');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200317_085816_category cannot be reverted.\n";

        return false;
    }
    */
}
