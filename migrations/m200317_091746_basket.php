<?php

use yii\db\Migration;

/**
 * Class m200317_091746_basket
 */
class m200317_091746_basket extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('basket', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'product_id' => $this->integer(),
            'count'=>$this->integer(),
        ]);
        $this->createIndex(
            'idx-basket-user_id',
            'basket',
            'user_id'
        );
        $this->addForeignKey(
            'fk-basket-user_id',
            'basket',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-basket-product_id',
            'basket',
            'product_id'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-basket-product_id',
            'basket'
        );

        $this->dropForeignKey(
            'fk-basket-user_id',
            'basket'
        );

        $this->dropIndex(
            'idx-basket-user_id',
            'basket'
        );
        $this->dropTable('basket');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200317_091746_basket cannot be reverted.\n";

        return false;
    }
    */
}
