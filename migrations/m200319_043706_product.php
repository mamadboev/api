<?php

use yii\db\Migration;

/**
 * Class m200319_043706_product
 */
class m200319_043706_product extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'c_id' => $this->integer(),
            'choosen'=>$this->boolean('false'),
            'status'=>$this->integer(1),
            'count'=>$this->integer(),
            'name'=>$this->string(),
            'price'=>$this->string(),
        ]);
        $this->createIndex(
            'idx-product-c_id',
            'product',
            'c_id'
        );
        $this->addForeignKey(
            'fk-product-c_id',
            'product',
            'c_id',
            'category',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-product-c_id',
            'product'
        );

        // drops index for column `author_id`
        $this->dropIndex(
            'idx-product-c_id',
            'product'
        );
        $this->dropTable('product');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200319_043706_product cannot be reverted.\n";

        return false;
    }
    */
}
