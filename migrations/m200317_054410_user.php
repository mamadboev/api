<?php

use yii\db\Migration;

/**
 * Class m200317_054410_user
 */
class m200317_054410_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

        $this->createTable('user', [
            'id' => $this->primaryKey(),
            'username' => $this->string(),
            'password' => $this->string(),
            'authKey' => $this->string(),
            'accessToken' => $this->string(),
            'created_at' => $this->date(),
            'updated_at' =>$this->timestamp(),
            'status'=>$this->integer(1),
        ]);
        $this->createIndex(
            'idx-user-id',
            'user',
            'id'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-user-id',
            'user'
        );
        $this->dropTable('user');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200317_054410_user cannot be reverted.\n";

        return false;
    }
    */
}
