<?php

namespace app\controllers;

use Yii;
use app\models\Product;
use yii\web\Controller;

use yii\rest\ActiveController;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends ActiveController
{
    public function actionCreateProduct()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $product = new Product();

        $product->scenario = Product:: SCENARIO_CREATE;

        $product->attributes = \yii::$app->request->post();

        if($product->validate())

        {
            $product->save();
            return array('status' => true, 'data'=> 'Product record is successfully updated');
        }
        else
        {
            return array('status'=>false,'data'=>$product->getErrors());
        }

    }


    public function actionGetProduct()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

        $product = Product::find()->all();

        if(count($product) > 0 )
        {
            return array('status' => true, 'data'=> $product);
        }
        else
        {
            return array('status'=>false,'data'=> 'No Product Found');
        }
    }
    public function actionUpdateProduct()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $attributes = \yii::$app->request->post();

        $product = Product::find()->where(['ID' => $attributes['id'] ])->one();
        if(count($product) > 0 )
        {
            $product->attributes = \yii::$app->request->post();
            $product->save();
            return array('status' => true, 'data'=> 'Product record is updated successfully');

        }
        else
        {
            return array('status'=>false,'data'=> 'No Product Found');
        }
    }

    public function actionDeleteProduct()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

        $attributes = \yii::$app->request->post();

        $product = Product::find()->where(['ID' => $attributes['id'] ])->one();

        if(count($product) > 0 )
        {
            $product->delete();
            return array('status' => true, 'data'=> 'Product record is successfully deleted');
        }
        else
        {
            return array('status'=>false,'data'=> 'No Product Found');
        }
    }
}
