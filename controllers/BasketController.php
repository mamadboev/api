<?php

namespace app\controllers;

use yii\rest\ActiveController;
use app\models\Basket;
use yii\rest\Serializer;
use yii\data\ActiveDataProvider;

class  BasketController extends  ActiveController
{
    public $modelClass = 'app\models\Basket';
}


