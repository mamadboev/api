<?php

namespace app\controllers;

use Yii;
use app\models\User;
use yii\rest\ActiveController;
use yii\rest\Serializer;
use yii\data\ActiveDataProvider;
use app\models\UserSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends ActiveController
{
    public $modelClass = 'app\models\User';

    public function actionIndex()
    {
        return new ActiveDataProvider([
            'query'=>User::find(),
        ]);
    }

}
