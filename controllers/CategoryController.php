<?php

namespace app\controllers;

use yii\rest\ActiveController;
use app\models\Category;

class CategoryController extends ActiveController
{

    public function actionCreateCategory()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $category = new Category();

        $category->scenario = Category:: SCENARIO_CREATE;

        $category->attributes = \yii::$app->request->post();

        if($category->validate())

        {
            $category->save();
            return array('status' => true, 'data'=> 'Category record is successfully updated');
        }
        else
        {
            return array('status'=>false,'data'=>$category->getErrors());
        }

    }


    public function actionGetCategory()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

        $category = Category::find()->all();

        if(count($category) > 0 )
        {
            return array('status' => true, 'data'=> $category);
        }
        else
        {
            return array('status'=>false,'data'=> 'No Category Found');
        }
    }
    public function actionUpdateCategory()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;
        $attributes = \yii::$app->request->post();

        $category = Category::find()->where(['ID' => $attributes['id'] ])->one();
        if(count($category) > 0 )
        {
            $category->attributes = \yii::$app->request->post();
            $category->save();
            return array('status' => true, 'data'=> 'Category record is updated successfully');

        }
        else
        {
            return array('status'=>false,'data'=> 'No Category Found');
        }
    }

    public function actionDeleteCategory()
    {
        \Yii::$app->response->format = \yii\web\Response:: FORMAT_JSON;

        $attributes = \yii::$app->request->post();

        $category = Category::find()->where(['ID' => $attributes['id'] ])->one();

        if(count($category) > 0 )
        {
            $category->delete();
            return array('status' => true, 'data'=> 'Category record is successfully deleted');
        }
        else
        {
            return array('status'=>false,'data'=> 'No Category Found');
        }
    }
}


